import 'package:flutter/material.dart';

void main() {
  runApp(WeatherApp());
}

class WeatherApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: buildAppBar(),
        body: buildBody(),
      ),

    );
  }
}

AppBar buildAppBar() {
  return AppBar(
    backgroundColor: Colors.transparent,
    elevation: 0,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.white,
      size: 30.0,
    ),

    title: Text(
      "Bangkok",
      style: TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.w400,
        fontSize: 35.0,
      ),
    ),
    centerTitle: true,
  );
}

Widget buildBody() {
  return Stack(
    children: <Widget>[
      Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                'https://images.unsplash.com/photo-1472552944129-b035e9ea3744?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8ODN8fGNsb3VkfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=600&q=60'),
            fit: BoxFit.cover,

          ),
        ),
        child: ListView(
          children: <Widget>[
            Container(
              height: 150,
              margin: EdgeInsets.all(20),
              child: ListView(
                children: [
                  Text(
                    "23°",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 70.0,
                      fontWeight: FontWeight.w300,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    "Partly Cloudy",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w300,
                      fontSize: 25.0,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    'H:31° L:21°',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w300,
                      fontSize: 25.0,

                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            Container(
              height: 200,
              child: ListView(
                children: <Widget>[
                  Container(
                    height: 180,
                    margin: EdgeInsets.all(10),
                    decoration: ShapeDecoration(
                      color: Colors.black12,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    child: ListView(
                      children: [
                        Container(
                          margin: EdgeInsets.all(15),
                          child: Text(
                            'Cloudy conditions from 02:00-05:00, with mostly clear conditions expected at 06:00.',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17.0,
                            ),
                          ),
                        ),
                        Divider(
                          color: Colors.grey.shade500,
                        ),
                        Container(
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(right: 20),
                                ),
                                Container(
                                  child: Text(
                                    "Now",
                                    style: TextStyle(
                                        fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 36),
                                ),
                                Text(
                                  "02",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 36),
                                ),
                                Text(
                                  "03",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 36),
                                ),
                                Text(
                                  "04",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 36),
                                ),
                                Text(
                                  "05",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 32),
                                ),
                                Text(
                                  "06",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                ),
                              ],
                            )
                        ),
                        Container(
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(right: 30),
                                ),

                                Container(
                                  child: Icon(
                                    Icons.cloud,
                                    color: Colors.white,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 48),
                                ),
                                Icon(
                                  Icons.cloud,
                                  color: Colors.white,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 37),
                                ),

                                Icon(
                                  Icons.cloud,
                                  color: Colors.white,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 37),
                                ),

                                Icon(
                                  Icons.cloud,
                                  color: Colors.white,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 37),
                                ),
                                Icon(
                                  Icons.cloud,
                                  color: Colors.white,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 32),
                                ),

                                Icon(
                                  Icons.sunny,
                                  color: Colors.white,
                                ),
                              ],
                            )
                        ),
                        Container(
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(right: 28),
                                ),
                                Container(
                                  child: Text(
                                    "23°",
                                    style: TextStyle(
                                        fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 39),
                                ),
                                Text(
                                  "22°",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 29),
                                ),
                                Text(
                                  "22°",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 29),
                                ),
                                Text(
                                  "21°",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 29),
                                ),
                                Text(
                                  "21°",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 21),
                                ),
                                Text(
                                  "21°",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                ),
                              ],
                            )
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 450,
              margin: EdgeInsets.all(10),
              decoration: ShapeDecoration(
                color: Colors.black12,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
              ),
              child: ListView(
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(top: 5),
                      padding: EdgeInsets.all(5),
                      child: Row(
                        children: [
                          Icon(
                            Icons.calendar_month,
                            color: Colors.grey,
                            size: 20,
                          ),
                          Text(
                            '  10 - DAY FORECAST',
                            style: TextStyle(
                              color: Colors.grey.shade500,
                              fontSize: 16.0,
                            ),
                          ),
                        ],
                      )),

                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 20),
                        ),

                        Container(
                          child: Text(
                            "Today",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 60),
                        ),
                        Icon(
                          Icons.cloud,
                          color: Colors.white,
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 60),
                        ),

                        Text(
                          "20°",
                          style: TextStyle(
                              fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 60),
                        ),

                        Text(
                          "32°",
                          style: TextStyle(
                              fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                        ),
                      ],
                    )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Wed",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 75),
                          ),
                          Icon(
                            Icons.cloud,
                            color: Colors.white,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "21°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "32°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Thu",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 80),
                          ),
                          Icon(
                            Icons.cloud,
                            color: Colors.white,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "21°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "32°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Fri",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 93),
                          ),
                          Icon(
                            Icons.cloud,
                            color: Colors.white,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "21°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "29°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Sat",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 86),
                          ),
                          Icon(
                            Icons.cloudy_snowing,
                            color: Colors.white,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "21°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "32°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Sun",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 80),
                          ),
                          Icon(
                            Icons.sunny,
                            color: Colors.white,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "21°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "32°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Mon",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 74),
                          ),
                          Icon(
                            Icons.sunny,
                            color: Colors.white,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "22°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "32°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Tue",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 82),
                          ),
                          Icon(
                            Icons.cloud,
                            color: Colors.white,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "23°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "32°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Wed",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 75),
                          ),
                          Icon(
                            Icons.cloud,
                            color: Colors.white,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "23°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "30°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Thu",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 80),
                          ),
                          Icon(
                            Icons.sunny,
                            color: Colors.white,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "22°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "32°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),









                ],
              ),
            ),



          ],
        ),
      ),
    ],
  );
}




